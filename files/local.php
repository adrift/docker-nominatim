<?php
 // version pins
 @define('CONST_Postgresql_Version', '9.6');
 @define('CONST_Postgis_Version', '2.5');
 // Website settings
 @define('CONST_Website_BaseURL', '/');
 // flatnode files https://nominatim.org/release-docs/latest/admin/Import-and-Update/#flatnode-files
 @define('CONST_Osm2pgsql_Flatnode_File', "/data/nominatim/flatnode.file");
?>
