#!/bin/bash

NOMINATIM_ROOT=${NOMINATIM_ROOT:="/data/nominatim"}
NOMINATIM_DATA_PATH=${NOMINATIM_DATA_PATH:="$NOMINATIM_ROOT/data"}
NOMINATIM_DATA_REGION=${NOMINATIM_DATA_REGION:="asia/maldives"}
NOMINATIM_PBF_URL=${NOMINATIM_PBF_URL:="http://download.geofabrik.de/$NOMINATIM_DATA_REGION-latest.osm.pbf"}

NOMINATIM_POSTGRESQL_DATA_PATH=${NOMINATIM_POSTGRESQL_DATA_PATH:="/var/lib/postgresql/9.3/main"}
NOMINATIM_DB_PASS=${NOMINATIM_DB_PASS:="osmisgreat1121"}

NOMINATIM_DATA_IMPORT_CPU_THREADS=${NOMINATIM_CPU_THREADS:="$(getconf _NPROCESSORS_ONLN)"}

if [ -f $NOMINATIM_DATA_PATH/$NOMINATIM_VERSION ]; then
    echo -e "\n\nNominatim version $NOMINATIM_VERSION found to already have been provisioned, skipping setup tasks\n\n"
else
    echo -e "\n\nNominatim version $NOMINATIM_VERSION NOT found to already have been provisioned, COMMENCING setup tasks!\n\n"
    # download osm region pbf load file
    # todo: support list of regions
    echo -e "\n\nDownloading pbf load file from $NOMINATIM_PBF_URL"
    curl -L $NOMINATIM_PBF_URL --create-dirs -o $NOMINATIM_DATA_PATH/load-file.osm.pbf

    service postgresql start

    # Import data
    useradd -m -p $NOMINATIM_DB_PASS nominatim
    sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='nominatim'" | grep -q 1 || sudo -u postgres createuser -s nominatim
    sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='www-data'" | grep -q 1 || sudo -u postgres createuser -SDR www-data
    sudo -u postgres psql postgres -c "DROP DATABASE IF EXISTS nominatim"
    $NOMINATIM_ROOT/utils/setup.php --osm-file $NOMINATIM_DATA_PATH/load-file.osm.pbf --all --threads $NOMINATIM_DATA_IMPORT_CPU_THREADS
fi

# Run Apache in the foreground
/usr/sbin/apache2ctl -D FOREGROUND
