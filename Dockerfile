FROM ubuntu:18.04 as builder

ENV DEBIAN_FRONTEND=noninteractive

ENV POSTGRES_VERSION=9.6

ENV LANG=C.UTF-8
ENV LANGUAGE=en_US:en
ENV LC_ALL en_US.UTF-8

ENV USERNAME=nominatim
ENV WORKDIR=/data/nominatim
ENV BRANCH=master
ENV GIT_DEPTH=1
ENV NOMINATIM_VERSION=3.4.1

###
### nominatim app & runtime prerequisites
###
RUN apt-get -y update && apt-get install -y --no-install-recommends curl gnupg2

RUN echo "deb http://apt.postgresql.org/pub/repos/apt bionic-pgdg main" >> /etc/apt/sources.list \
 && curl -fsSL http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add - \
 && apt-get -y update -qq \
 && apt-get -y install locales \
 && locale-gen en_US.UTF-8 \
 && update-locale LANG=en_US.UTF-8 \
 && apt-get install -y build-essential \
                       cmake \
                       g++ \
                       libboost-dev \
                       libboost-system-dev \
                       libboost-filesystem-dev \
                       libexpat1-dev \
                       zlib1g-dev \
                       libxml2-dev \
                       libbz2-dev \
                       libpq-dev \
                       libgeos-dev \
                       libgeos++-dev \
                       libproj-dev \
                       postgresql-server-dev-$POSTGRES_VERSION \
                       php \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /tmp/* /var/tmp/*

RUN apt-get -y update \
 && apt-get install -y python3-pip \
                       python3-dev \
 && ln -s /usr/bin/python3 /usr/bin/python \
 && pip3 install --upgrade pip \
 && pip3 install osmium \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /data && cd /data \
 && curl -fsSL http://www.nominatim.org/release/Nominatim-$NOMINATIM_VERSION.tar.bz2 -O \
 && tar xf Nominatim-$NOMINATIM_VERSION.tar.bz2 \
 && rm Nominatim-$NOMINATIM_VERSION.tar.bz2 \
 && mv Nominatim-$NOMINATIM_VERSION nominatim \
 && chown -R root:root nominatim/ \
 && cd nominatim \
 && mkdir build \
 && cd build \
 && cmake .. \
 && make

FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

ENV POSTGRES_VERSION=9.6
ENV LANG=C.UTF-8
ENV LANGUAGE=en_US:en
ENV LC_ALL en_US.UTF-8

ENV USERNAME=nominatim
ENV WORKDIR=/data/nominatim
ENV BRANCH=master
ENV NOMINATIM_VERSION=3.4.1

RUN apt-get -y update && apt-get install -y --no-install-recommends curl \
                                                                    gnupg2 \
                                                                    locales \
                                                                    ca-certificates \
                                                                    sudo \
 && echo 'en_GB.UTF-8 UTF-8' >> /etc/locale.gen \
 && echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen \
 && locale-gen \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /tmp/* /var/tmp/*

RUN echo "deb http://apt.postgresql.org/pub/repos/apt bionic-pgdg main" >> /etc/apt/sources.list \
 && curl -fsSL http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add - \
 && apt-get -y update \
 && apt-get install -y -qq --no-install-recommends locales \
 && locale-gen en_US.UTF-8 \
 && update-locale LANG=en_US.UTF-8 \
 && apt-get install -y -qq --no-install-recommends \
    postgresql-$POSTGRES_VERSION \
    postgresql-server-dev-$POSTGRES_VERSION \
    postgresql-9.6-postgis-2.5 \
    postgresql-contrib-$POSTGRES_VERSION \
    apache2 \
    php \
    php-pgsql \
    libapache2-mod-php \
    php-pear \
    php-db \
    php-intl \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /tmp/* /var/tmp/*

RUN apt-get -y update \
 && apt-get install -y python3-pip \
                    python3-dev \
 && ln -s /usr/bin/python3 /usr/bin/python \
 && pip3 install --upgrade pip \
 && pip3 install osmium \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir -p $WORKDIR

# Copy the application from the builder image
COPY --from=builder $WORKDIR/build $WORKDIR

WORKDIR $WORKDIR

COPY files/local.php /data/nominatim/build/settings/local.php
COPY files/apache-nominatim.conf /etc/apache2/sites-enabled/000-default.conf

# Allow remote connections to PostgreSQL
RUN echo "host all  all    127.0.0.1/32  trust" >> /etc/postgresql/$POSTGRES_VERSION/main/pg_hba.conf \
 && echo "host all  all    172.0.0.0/8  trust" >> /etc/postgresql/$POSTGRES_VERSION/main/pg_hba.conf \
 && echo "host all  all    10.0.0.0/8  trust" >> /etc/postgresql/$POSTGRES_VERSION/main/pg_hba.conf \
 && echo "listen_addresses='*'" >> /etc/postgresql/$POSTGRES_VERSION/main/postgresql.conf

COPY files/docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh \
 && chown -R www-data:www-data $WORKDIR/build

ENTRYPOINT ["/docker-entrypoint.sh"]
